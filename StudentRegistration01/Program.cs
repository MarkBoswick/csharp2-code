﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentRegistration01
{
    class Program
    {
        static void Main(string[] args)
        {

            IRegisterStudent registerStudentPT = new RegisterPartTime();
            registerStudentPT.RegisterStudent(new PartTimeStudent(45, "FNamePart ", "LNamePart"));
            registerStudentPT.RegisterStudent(new PartTimeStudent(46, "FNamePart01 ", "LNamePart01"));
            registerStudentPT.getStudents();

            IRegisterStudent registerStudentFT = new RegisterFullTime();
            registerStudentFT.RegisterStudent(new FullTimeStudent(100, "FNameFull ", "LNameFull"));
            registerStudentFT.RegisterStudent(new FullTimeStudent(101, "FNameFull01 ", "LNameFull01"));
            registerStudentFT.getStudents();


        }
    }
}
