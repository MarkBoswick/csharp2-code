﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentRegistration01
{
    class RegisterFullTime : IRegisterStudent
    {
        List<FullTimeStudent> listOfFullTimeStudents = new List<FullTimeStudent>();
        public void getStudents()
        {

            foreach (var std in listOfFullTimeStudents)
            {
                Console.WriteLine(std.FirstName + " " + std.LastName);
            }
            // return listOfPartTimeStudents;
        }

        public void RegisterStudent(Student student)
        {
            Console.WriteLine("This will register FullTime Student: " + student.FirstName);
            listOfFullTimeStudents.Add((FullTimeStudent)student);

        }
    }
}
