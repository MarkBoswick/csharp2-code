﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentRegistration01
{
    class RegisterPartTime : IRegisterStudent
    {
        List<PartTimeStudent> listOfPartTimeStudents = new List<PartTimeStudent>();
        public void getStudents()
        {

            foreach (var std in listOfPartTimeStudents)
            {
                Console.WriteLine(std.FirstName + " " + std.LastName);
            }
            // return listOfPartTimeStudents;
        }

        public void RegisterStudent(Student student)
        {
            Console.WriteLine("This will register PartTime Student: " + student.FirstName);
            listOfPartTimeStudents.Add((PartTimeStudent)student);

        }
    }
}
