﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentRegistration01
{
    interface IRegisterStudent
    {
        void getStudents();

        void RegisterStudent(Student student);
    }
}
