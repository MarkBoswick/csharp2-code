﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session07_Task
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //int location; 
            int location = richTextBox1.Find(textBox1.Text);
            int strlength = textBox1.Text.Length;
            richTextBox1.Select(location, strlength);
            richTextBox1.SelectionColor = Color.Red;

        }
    }
}
