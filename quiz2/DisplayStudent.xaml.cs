﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace quiz2
{
    /// <summary>
    /// Interaction logic for DisplayStudent.xaml
    /// </summary>
    public partial class DisplayStudent : Window
    {
        List<Student> students = new List<Student>();
        public DisplayStudent()
        {
            InitializeComponent();

            //List<Student> students = new List<Student>();
            students.Add(new Student() { Id = 1, FName = "AA", LName = "LLLL", IsRegistered = true });
            students.Add(new Student() { Id = 2, FName = "BB", LName = "HGHHH", IsRegistered = false });
            students.Add(new Student() { Id = 3, FName = "CC", LName = "KKKKKK", IsRegistered = true });

            dGStudents.ItemsSource = students;

           // lvUsers.ItemsSource = students;

        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            tbxId.Text = "";
            tbxFName.Text = "";
            tbxLName.Text = "";
            cbxRegistered.IsChecked = false;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("This adds student to the list");

            // cbxRegistered  needs to be converted I put true so my program would compile for you
            students.Add(new Student() { Id = int.Parse(tbxId.Text), FName = tbxFName.Text, LName = tbxLName.Text, IsRegistered = true });
        }
        
    }

    public class Student
    {
        public int Id { get; set; }
        public string FName { get; set; }

        public string LName { get; set; }

        public bool IsRegistered { get; set; }
    }
}
