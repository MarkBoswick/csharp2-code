﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment02_01
{
    class Program
    {
        static void Main(string[] args)
        {
           
            NameClass<string> nameClass = new NameClass<string>("Mark");

            // call the method -- calling with lowercase letter and printing in method
            string val = nameClass.genericMethod("mark");

            // also during the call to the method a value is return, which is now printed
            Console.WriteLine("Value returned is: " + val);
        }
    }
}


class NameClass<T>
{
    private T strName;

    public NameClass(T value)
    {
        strName = value;
    }

    public T genericMethod(T genericParameter)
    {
       
        Console.WriteLine("Print name in method: " + strName);

        return genericParameter;
    }

}
