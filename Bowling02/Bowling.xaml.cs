﻿using Bowling02.modelview;
using Bowling02.service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Bowling02
{
    /// <summary>
    /// Interaction logic for Bowling.xaml
    /// </summary>
    public partial class Bowling : Window
    {
        public readonly static BowlerService bs = new BowlerService();
        public static ModelView mv = new ModelView();
        public Bowling()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //bring customers from db, but I want  bowler and score together
            Bowling.mv.Bowlers = bs.GetBowlers();
            //Bowling.mv.
            DataContext = Bowling.mv;



        }
    }
}
