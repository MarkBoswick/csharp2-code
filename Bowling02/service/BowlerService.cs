﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Bowling02.service
{
    public class BowlerService
    {
        // instantiate and object of datacontext
        public Guid AddBowler(Bowler bowler)
        {
            try
            {
                using (var context = new BowlingEntities())
                {
                    context.Bowlers.Add(bowler);
                    context.SaveChanges();
                    return bowler.Id;
                }
            }
            catch
            {
                return Guid.Empty;
            }
        }
            public List<Bowler> GetBowlers()
            {
               using (var context = new BowlingEntities())
                {
                var query = from bowler in context.Bowlers
                            select bowler;
                return query.ToList<Bowler>();

                }
            }
        


    }
}
