﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling02.modelview
{

    public class ModelView : INotifyPropertyChanged
    {
        public List<Bowler> bowlers;
        public List<Bowler> Bowlers
        {
            get { return bowlers; }
            set
            {
                bowlers = value;
                OnPropertyChanged("Bowlers");
            }
        }

        private void OnPropertyChanged(string changedProprtyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(changedProprtyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
    
}
