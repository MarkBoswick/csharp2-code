﻿using Bowling02.service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Bowling02
{
    /// <summary>
    /// Interaction logic for BowlingAddBox.xaml
    /// </summary>
    public partial class BowlingAddBox : UserControl
    {
        public BowlerService bowlerService = new BowlerService();
        public BowlingAddBox()
        {
            InitializeComponent();
        }

        //private void btnReset_Click(object sender, RoutedEventArgs e)
        //{

        //}

        private void btnResetAddForm_Click(object sender, RoutedEventArgs e)
        {
            txtId.Clear();
            txtFirstName.Clear();
            txtLastName.Clear();
            txtPhone.Clear();
            txtEmail.Clear();
            txtStreet.Clear();
            txtState.Clear();
            txtCity.Clear();
            txtZip.Clear();
        }

        private void btnSaveAddForm_Click(object sender, RoutedEventArgs e)
        {
            // take the values within each entity
            // call a service
            Bowler bowler = new Bowler()
            {   
                Id = Guid.NewGuid(),
                FirstName = txtFirstName.Text,
                LastName = txtLastName.Text,
                Phone = txtPhone.Text,
                Email = txtEmail.Text,
                Street = txtStreet.Text,
                State = txtState.Text,
                City = txtCity.Text,
                Zip = txtZip.Text
            };

           
            Guid result = bowlerService.AddBowler(bowler);

            //MessageBox.Show($"Before if :{result.ToString()}  {bowler.FirstName} ", "success", MessageBoxButton.OK, MessageBoxImage.Information);




            if (result != Guid.Empty) //SUCCESS
                
                //if (result != 00000000-0000-0000-0000-000000000000) //SUCCESS
            {
                btnResetAddForm_Click(sender, null);
                MessageBox.Show($"Bowler {bowler.FirstName} {bowler.LastName}  created with ID :{result.ToString()}", "success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
               MessageBox.Show($"Bowler not added for ID :{result.ToString()}", "failure", MessageBoxButton.OK, 
                   MessageBoxImage.Error);
            }
        }


  // private bool validateInput() 
            // {
            // }

    }
}
