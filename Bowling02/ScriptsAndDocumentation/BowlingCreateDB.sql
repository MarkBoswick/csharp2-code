﻿
use master
;
go 



/* create database */
create database Bowling
on primary
(
	-- 1) rows data logical filename
	name = 'Bowling',
	-- 2) rows data initial file size
	size = 20MB,
	-- 3) rows data auto growth size
	filegrowth = 5MB,
	-- 4) rows data maximum file size
	maxsize = 500MB, -- unlimited
	-- 5) rows data path and file name
	--filename = 'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER01\MSSQL\DATA\MySchooldb.mdf'
	filename = 'F:\DataBases\Bowling.mdf'
)
log on
(
	-- 1) log data logical filename
	name = 'Bowling_log',
	-- 2) log initial file size (1/4th of the data file size)
	size = 5MB,
	-- 3) log auto growth size
	filegrowth = 10%,
	-- 4) log maximum file size
	maxsize = 100MB, -- unlimited
	-- 5) log path and file name
	filename = 'F:\DataBases\Bowling_log.ldf'
)
;
go

/* return information about Library database using system stored procedure */
execute sp_helpdb
;
go

execute sp_helpdb Bowling
;
go

-- return the user name, database name, and the server name
select 
	USER_NAME() as 'User Name',
	DB_NAME() as 'Database Name',
	@@SERVERNAME as 'Server Name'
;
go




---- after above commands exit sqlserver and come back in Bowling DataBase will then display

----------------------------------------------------------------------

USE [Bowling]

PRINT '
*** Bowler Table Generation ***'

/*** Table [dbo].[Boowler]  ***/
IF OBJECT_ID('[dbo].[Bowler]', 'U') IS NOT NULL
DROP TABLE [dbo].[Bowler]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- this is needed if you are recreating the table for a second time


--ALTER TABLE Bowler
--drop constraint PK_Bowler;

--ALTER TABLE Score
--drop constraint PK_Score;
--drop constraint fk_ID;


CREATE TABLE [dbo].[Bowler](
[Id] [uniqueidentifier] NOT NULL,
[FirstName] [nvarchar](50) NULL,
[LastName] [nvarchar](50) NULL,
[Phone] [nvarchar](100) NULL,
[Email] [nvarchar](255) NULL,
[Street] [nvarchar](100) NULL,
[City] [nvarchar](100) NULL,
[State] [nvarchar](2) NULL,
[Zip] [nvarchar](10) NULL,
CONSTRAINT [PK_Bowler] PRIMARY KEY CLUSTERED
([Id] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF,
ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('7462c7c8-e24c-484a-8f93-013f1c479615','Derek','Puckett','(954) 594-9355','derek.puckett@vulputate.net','P.O. Box 914, 9990 Dapibus St.','Quam','OH','55154');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('8f64b183-a881-42f5-9c1d-013f1c479616','Bernard','Russell','(203) 652-0465','bernard.russell@torquentper.com','324-6843 Dolor Ave','Quis','FL','28034');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('5a2ff04d-e9ae-4de1-9f4e-013f1c479617','Jordan','Jimenez','(265) 520-8354','jordan.jimenez@variusorciin.co.uk','Ap #370-9242 Sed, Ave','Lorem','OR','88091');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('11da4696-cea3-4a6d-8e83-013f1c479618','Jordan','Holloway','(761) 224-2078','jordan.holloway@felisullamcorper.edu','Ap #128-7062 Viverra. Road','Penatibus','PA','82092');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('c56f1005-28c6-454d-9525-013f1c479619','Dorian','Hurley','(691) 714-1449','dorian.hurley@ipsumprimis.org','3100 Nunc St.','Nam','IL','57878');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('975c4a14-fae9-4e80-b6e5-013f1c47961a','Regan','Wright','(126) 144-1855','regan.wright@ullamcorpernislarcu.edu','Ap #701-4141 Ante. Rd.','Dictum','KS','82560');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('009a0942-b1fd-4618-a7de-013f1c47961b','Hedda','Kemp','(147) 741-1696','hedda.kemp@nunc.ca','P.O. Box 880, 3605 Nec, Ave','Dolor City','MS','56403');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('718d7784-c177-4c54-a2d3-013f1c47961d','Jordan','Kelly','(859) 624-7317','jordan.kelly@lorem.org','P.O. Box 866, 3666 Phasellus Rd.','Rutrum','MN','69800');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('0ff9f486-5ac6-40a8-a25c-013f1c47961e','Cheryl','Massey','(601) 563-9471','cheryl.massey@dictumplacerat.edu','Ap #171-7323 Mattis Av.','Erat','OK','02283');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('651d1aa8-e7f6-48f6-ae92-013f1c47961f','Erich','Barron','(880) 947-3420','erich.barron@Loremipsumdolor.net','Ap #442-177 Volutpat Avenue','Egestas City','CT','42871');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('0e2a4fee-380b-42d3-8b78-013f1c479620','Brianna','Allen','(298) 393-0372','brianna.allen@InfaucibusMorbi.com','P.O. Box 902, 5276 Semper St.','Rhoncus City','ME','86063');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('db929abb-e670-4aa8-9ee2-013f1c479621','Chastity','Robbins','(437) 367-6613','chastity.robbins@Donecnonjusto.net','2295 Elit. Avenue','Non','TN','02263');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('dde19a04-84b1-4452-9612-013f1c479622','Wallace','Bates','(574) 847-4231','wallace.bates@sem.org','712-6613 Orci Ave','Nulla City','MS','05332');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('21829039-ee75-4865-b84f-013f1c479623','Molly','Frank','(310) 705-4577','molly.frank@Inlorem.edu','2277 Donec Rd.','Mollis','KY','75839');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('fa539487-c572-4045-a6bf-013f1c479624','Demetrius','Langley','(657) 854-8183','demetrius.langley@Nam.edu','4814 Nunc. St.','Nec','AK','39498');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('75b02196-60d3-476c-8c42-013f1c479625','Reese','Stephens','(134) 602-2513','reese.stephens@quam.ca','580-9933 Ornare St.','Fusce','MT','68017');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('7f2aa755-ecb0-4f80-ad01-013f1c479626','Shelley','Weber','(692) 253-4895','shelley.weber@Fusce.org','Ap #156-4219 Et, Av.','Proin','IA','66768');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('8f3b5a9f-85fc-495d-901b-013f1c479627','Jennifer','Leblanc','(814) 120-4683','jennifer.leblanc@Fuscefermentumfermentum.ca','779-8730 Auctor. Av.','Libero','IA','79546');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('046f6c1f-b8b5-4e5e-aa7c-013f1c479628','Idona','West','(220) 623-6795','idona.west@metusIn.org','P.O. Box 734, 7881 Posuere St.','Utville','MT','93058');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('c9a76e2c-a39e-4c4a-ac4a-013f1c479629','Melyssa','Weaver','(881) 844-1111','melyssa.weaver@libero.edu','P.O. Box 930, 2346 Turpis. Rd.','Massa City','LA','09104');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('e5ebf143-3820-4600-bac9-013f1c47962a','Althea','Patrick','(485) 679-0902','althea.patrick@vitaesemper.edu','Ap #750-567 Ac St.','Eget','NV','56076');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('01d4652b-cc00-4c57-90f7-013f1c47962b','John','Russell','(496) 953-8629','john.russell@loremauctor.net','1918 Etiam Avenue','Auctor','IL','39599');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('3bcfcff9-ea4e-4008-98c9-013f1c47962c','Dylan','Mitchell','(500) 479-9175','dylan.mitchell@aliquetPhasellus.org','P.O. Box 773, 4409 Sit Rd.','Nec','VT','17367');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('981c8345-0640-4a79-903d-013f1c47962d','Georgia','Webster','(480) 427-2180','georgia.webster@utcursusluctus.edu','Ap #298-6821 Ut, Avenue','Risus City','MO','45872');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('ba33b6e0-3e40-4c79-b581-013f1c47962e','Lee','Burris','(399) 321-0539','lee.burris@vel.com','P.O. Box 876, 8449 Nostra, Rd.','Lorem City','WY','93880');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('ea95f2e9-c91b-45b3-b2c4-013f1c47962f','Cameron','Head','(233) 266-3616','cameron.head@anuncIn.com','1306 Proin St.','Sollicitudin','ID','89918');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('14c12e59-d802-4a0e-baff-013f1c479630','Oliver','Dale','(114) 445-0087','oliver.dale@tinciduntvehicula.com','308-1383 Non Street','Ornare','KY','30821');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('0a18f73f-0255-454e-9c44-013f1c479631','Naida','Whitney','(470) 815-3162','naida.whitney@mollisvitaeposuere.co.uk','P.O. Box 399, 6339 Nisi Road','Adipiscing City','LA','10892');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('135ec153-9c71-4df8-9778-013f1c479632','Wing','Miller','(333) 414-9465','wing.miller@sodalespurus.net','P.O. Box 483, 8826 Vel Rd.','Fermentum','CT','68647');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('43963d60-81d3-4300-9671-013f1c479633','Vernon','Hammond','(417) 758-1428','vernon.hammond@eleifendvitae.org','3274 Nec, Rd.','Netus','MS','74761');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('f3e6cd21-68b1-4470-b0eb-013f1c479634','Carla','Hall','(157) 325-7072','carla.hall@eu.co.uk','P.O. Box 311, 7310 Quis Ave','Diam','CT','40026');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('3bd069e9-4c5a-45ad-af0a-013f1c479635','Castor','Hartman','(129) 841-3220','castor.hartman@nisinibh.co.uk','Ap #588-5375 Suspendisse Street','At City','KY','94906');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('730fa9e4-0f3e-4766-a265-013f1c479636','Lewis','Clemons','(163) 948-1803','lewis.clemons@miloremvehicula.com','Ap #524-5510 Aliquam St.','Ipsum','TN','97887');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('42a77144-d45d-48f7-97bb-013f1c479637','Jolene','Holland','(950) 445-1642','jolene.holland@Utnecurna.co.uk','6650 Enim. Av.','Tristique','HI','18696');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('e4a5c596-0d28-460e-aa67-013f1c479638','Kasimir','Oneill','(296) 709-1618','kasimir.oneill@malesuadaaugueut.com','769-925 Vulputate, Street','Orci City','TN','82341');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('31a34f9d-47a8-4428-a00d-013f1c479639','Aileen','Alvarez','(829) 590-4166','aileen.alvarez@In.edu','812-4435 Aliquet. Ave','Lectus','MD','86721');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('b97f1246-544e-49b7-a332-013f1c47963a','Taylor','Delaney','(274) 223-1647','taylor.delaney@facilisisnon.com','9403 Nullam Av.','Justo City','VA','48216');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('919e7af7-0af6-491e-8246-013f1c47963b','Jillian','Farmer','(574) 315-4571','jillian.farmer@ligulaAliquam.edu','Ap #749-5159 Sem Rd.','Malesuada City','KS','97719');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('b89c43e5-a220-42ce-9b25-013f1c47963c','Germane','Noel','(283) 615-2609','germane.noel@utpellentesque.org','1410 Blandit St.','Commodo','AZ','23691');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('8af152a0-1410-45e5-88ac-013f1c47963d','Rigel','Tran','(540) 387-4490','rigel.tran@lectusrutrumurna.org','2608 Egestas. Avenue','Ipsum City','MS','47373');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('19d35bbc-e711-4dff-8e63-013f1c47963e','Fredericka','Sweet','(223) 636-5019','fredericka.sweet@posuerevulputate.edu','P.O. Box 811, 2885 Et, Road','Sed City','WA','50088');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('530e0cae-0025-482a-becb-013f1c47963f','Jameson','Dale','(764) 227-7789','jameson.dale@Nam.net','P.O. Box 467, 8040 Diam St.','Bibendum','MA','73582');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('5b9a6abf-6d8f-4721-842b-013f1c479640','Jared','Gallagher','(529) 932-7105','jared.gallagher@placerataugue.co.uk','3544 Ut, Rd.','Curabitur','HI','71517');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('617c046e-b8f3-4028-8da5-013f1c479641','Helen','Harper','(378) 568-8959','helen.harper@penatibus.ca','Ap #805-4319 Arcu Rd.','Metus City','MO','84235');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('d241fdf4-3507-4529-bb46-013f1c479642','Althea','Rosario','(925) 871-6501','althea.rosario@et.net','P.O. Box 616, 6012 In Av.','Sem','WY','75609');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('912d126c-13da-4d71-be7b-013f1c479643','Courtney','Bray','(713) 390-4565','courtney.bray@Nuncsed.ca','519-4610 Nonummy. Av.','Ullamcorper City','MO','29699');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('426ebaf1-4169-4b84-9805-013f1c479644','Kitra','Crawford','(123) 442-5981','kitra.crawford@justo.ca','Ap #294-6210 Integer St.','Dolor','HI','15675');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('1e2f8a25-8ae9-4ce8-8b64-013f1c479645','Marsden','Freeman','(775) 110-3692','marsden.freeman@utmiDuis.com','146-9891 Sit Ave','Aenean','OK','81759');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('04391a5f-9a1a-40c2-b811-013f1c479646','Deanna','Cash','(398) 800-8468','deanna.cash@sagittis.org','P.O. Box 144, 564 Hendrerit Avenue','Nulla','MT','49214');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('c494f0e5-d728-4ecb-b2fa-013f1c479647','Cheryl','Harvey','(643) 524-5182','cheryl.harvey@imperdietornareIn.co.uk','2987 Arcu. Av.','Amet','MA','01803');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('fec96cb2-57bb-4092-863b-013f1c479648','Alexis','Kirkland','(641) 976-2223','alexis.kirkland@rutrumeu.edu','P.O. Box 560, 4261 Pede Rd.','Amet','UT','94584');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('ccd7ebd7-8157-4b45-801f-013f1c479649','Tana','Spence','(942) 240-7629','tana.spence@arcuCurabitur.net','715-6134 Ac St.','Pharetra City','OH','11469');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('db92be00-e192-4523-be7b-013f1c47964a','Vielka','Lyons','(890) 104-1814','vielka.lyons@risusInmi.ca','Ap #939-3921 Tempus Ave','Enim City','FL','37684');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('0f9d7ed9-483d-4d68-b947-013f1c47964b','Macey','Quinn','(946) 156-4745','macey.quinn@massarutrummagna.org','P.O. Box 130, 5971 Montes, Rd.','Non City','OR','16074');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('e6e6fa3a-5f00-4254-a076-013f1c47964c','Jeremy','Henson','(348) 943-5910','jeremy.henson@ametlorem.org','P.O. Box 129, 5651 Augue, Rd.','Donec','IN','87595');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('2da740b7-9467-46d1-aca6-013f1c47964d','Avram','Hamilton','(938) 823-1723','avram.hamilton@acipsumPhasellus.co.uk','181-761 Libero St.','Curabitur','AZ','95454');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('6f424b6d-063c-4b85-b2c6-013f1c47964e','Wanda','Mccoy','(889) 761-0883','wanda.mccoy@in.co.uk','3903 Sollicitudin Avenue','Adipiscing','VT','77331');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('2a68e04e-da3c-401b-807b-013f1c47964f','Riley','Savage','(136) 924-6445','riley.savage@vitaeorciPhasellus.net','Ap #538-9663 Enim Avenue','Dictum','OH','52785');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('750460f7-194c-4896-b51f-013f1c479650','Shelby','Mills','(490) 193-4579','shelby.mills@convallisligulaDonec.edu','P.O. Box 377, 8310 Id Road','Fusce','WI','09577');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('6879c677-b50f-4d50-8de4-013f1c479651','Beck','Morrison','(445) 454-7004','beck.morrison@orciluctus.co.uk','P.O. Box 777, 6014 Ridiculus Street','At City','NV','38096');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('870a8e13-b246-407d-82ed-013f1c479652','Reed','Albert','(324) 969-3404','reed.albert@nuncrisus.net','P.O. Box 637, 8360 Nam St.','Inville','AK','60569');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('c6cf303f-a47b-40e9-900a-013f1c479653','Daphne','Hooper','(956) 442-3863','daphne.hooper@Inat.edu','P.O. Box 198, 7996 Nunc St.','Fringilla City','TX','53410');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('ede004e8-e5cd-4aab-a13c-013f1c479654','Georgia','Haney','(429) 164-3030','georgia.haney@Donecelementum.org','Ap #802-583 Montes, Avenue','Laoreet','HI','83124');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('80b2ff13-7f6f-47c3-8502-013f1c479655','Brenna','Morrow','(859) 865-2065','brenna.morrow@Etiam.co.uk','2219 Cursus Rd.','Amet','CA','96527');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('f22e1c05-29bf-4305-9d9a-013f1c479656','Reuben','Patrick','(945) 533-1951','reuben.patrick@quis.com','4997 Quam, Rd.','Tincidunt City','GA','37692');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('ba818b5c-c1f5-474f-b218-013f1c479657','Vladimir','Pratt','(166) 510-8527','vladimir.pratt@felis.ca','Ap #549-3144 Lobortis Rd.','Nulla','VA','82051');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('af43391d-5eca-4715-bfe5-013f1c479658','Echo','Larsen','(921) 573-6588','echo.larsen@interdum.com','Ap #560-469 Non Rd.','Nec','IL','35131');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('6a3b0efb-4c23-43d3-a298-013f1c479659','Emmanuel','Nguyen','(629) 876-2320','emmanuel.nguyen@odio.edu','Ap #879-7971 Nec, Street','Placerat City','TN','61074');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('13144c4f-9a3f-487e-95b2-013f1c47965a','Chandler','Barrett','(282) 169-7830','chandler.barrett@famesac.edu','301-1867 Quis Street','Nec','AL','54062');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('c1592825-a41c-4a7f-b6a2-013f1c47965b','Kyle','Rodgers','(376) 583-5528','kyle.rodgers@uterat.com','Ap #791-9237 Convallis Ave','Interdum','LA','74380');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('90ce645e-4991-4d23-83cc-013f1c47965c','Kermit','Hamilton','(880) 539-7849','kermit.hamilton@sem.com','608 Nisi Rd.','Volutpat City','LA','26569');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('8c6a8389-23d1-4570-abd7-013f1c47965d','Halee','Phillips','(575) 380-6774','halee.phillips@dis.co.uk','955-4287 Consectetuer Rd.','Mauris City','OR','25882');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('66aa6f49-f761-4097-922e-013f1c47965e','Rogan','Davidson','(929) 558-5253','rogan.davidson@enim.edu','Ap #544-2072 Et Street','Neque City','FL','32111');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('e2e7932b-72bc-41d2-aedf-013f1c47965f','Grady','Abbott','(432) 388-1417','grady.abbott@acturpis.net','2749 Metus Avenue','Suspendisse','WA','37668');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('d2c20582-40ef-4a60-a0a3-013f1c479660','Shad','Rocha','(281) 514-3068','shad.rocha@duiFusce.net','404-6753 Facilisis Ave','Aville','KS','01567');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('a2800de6-e629-4dbb-8259-013f1c479661','Hanna','Lawson','(653) 705-1457','hanna.lawson@Aliquam.ca','364-6175 Tincidunt Street','Pharetra','VA','80436');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('042edc65-192e-40c2-8dc9-013f1c479662','Elliott','Rhodes','(283) 653-0786','elliott.rhodes@lectus.co.uk','230-7699 Scelerisque Rd.','Phasellus','GA','29773');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('c2f5bcfe-dbb2-45af-9e61-013f1c479663','Iona','Baker','(168) 424-7326','iona.baker@ataugueid.org','171 Vitae St.','Cursus City','IA','23120');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('84874157-3434-48cc-b659-013f1c479664','Sloane','Knox','(585) 279-3277','sloane.knox@nislsem.edu','8391 Imperdiet, Rd.','Tincidunt City','OH','97338');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('ad897050-6f7e-4b2c-ac3c-013f1c479665','Kadeem','Hawkins','(709) 650-4546','kadeem.hawkins@consectetuer.net','Ap #111-5843 Massa. Ave','Auctor','TX','43177');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('b134085a-e4d8-4d5a-91df-013f1c479666','Alisa','Farmer','(272) 138-1745','alisa.farmer@semmagnanec.org','P.O. Box 292, 1209 Quis St.','Penatibus','FL','11929');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('51b09a06-8d20-4a75-9078-013f1c479667','Marcia','French','(270) 574-3828','marcia.french@imperdietornare.org','Ap #110-902 Ornare Av.','Id City','HI','17800');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('e414e5dc-e3a4-4116-850b-013f1c479668','Richard','Pennington','(494) 542-9811','richard.pennington@posuerecubiliaCurae;.co.uk','8682 Dignissim St.','Sagittis City','MA','86664');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('e375b684-d152-49c7-a640-013f1c479669','Ori','Pickett','(341) 395-7156','ori.pickett@justo.ca','6924 Eleifend Av.','Tellus','KY','43609');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('f805ac2d-caba-41f4-b52a-013f1c47966a','Gary','Lott','(708) 211-0792','gary.lott@elitAliquam.co.uk','379-6043 Ut Rd.','Magna','TX','93764');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('bab58008-51db-4a69-84d1-013f1c47966b','Raven','Wells','(950) 862-9798','raven.wells@nonmagna.com','5849 Mauris St.','Diam','AK','29567');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('9d8c5bc2-769a-4ab9-9ace-013f1c47966c','Charlotte','Becker','(510) 118-7374','charlotte.becker@quamCurabitur.co.uk','P.O. Box 235, 8495 Risus. Avenue','Quam','WA','37294');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('d7e0378c-77ab-403c-a5a1-013f1c47966d','Alexa','Butler','(552) 554-3280','alexa.butler@facilisiseget.co.uk','7239 Et, Road','Tempor','TX','00883');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('d634a1a7-97c3-4373-bf55-013f1c47966e','Pascale','Fowler','(487) 913-4567','pascale.fowler@pedenec.org','3192 Laoreet Rd.','Integer','IA','10919');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('f4bce6d7-6a19-41a3-9cd8-013f1c47966f','Sandra','Kirk','(618) 597-6222','sandra.kirk@cursusvestibulumMauris.ca','Ap #910-8612 Quisque Avenue','Utville','MD','74665');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('0a54e14d-8deb-41d3-b00f-013f1c479670','Kay','Kirk','(116) 965-7370','kay.kirk@Class.com','P.O. Box 286, 5025 Donec Rd.','Interdum','LA','57214');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('14f87722-8c15-47c1-99f0-013f1c479671','Shannon','Nguyen','(998) 695-1689','shannon.nguyen@Curae;Donec.net','957-6278 Pede. Road','Erat','AK','64486');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('9c8f68ec-f65b-4d16-a919-013f1c479672','Silas','Pate','(376) 235-6524','silas.pate@sedsapienNunc.ca','P.O. Box 929, 8134 Phasellus Av.','Aliquet','HI','33271');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('23bcfc95-a3cd-4913-80b0-013f1c479673','Shaine','Mullins','(814) 528-1833','shaine.mullins@mauris.net','Ap #133-8301 Ut, Avenue','Luctus','FL','21849');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('e4d8bc78-fbbd-4504-945c-013f1c479674','Anastasia','Gill','(594) 147-8651','anastasia.gill@Aliquamornarelibero.com','P.O. Box 508, 2957 Tristique Street','Quis City','NV','42137');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('03256bb2-877d-4207-997f-013f1c479675','Alice','Herrera','(153) 558-2447','alice.herrera@montesnasceturridiculus.net','636 Mi Street','Egestas','OH','62494');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('e6d2a1d3-ceb4-4df5-8a63-013f1c479676','Simone','Mclean','(526) 161-6430','simone.mclean@auctorquis.co.uk','4595 Sem St.','Odio City','MI','38863');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('0b98919e-c77a-47ee-a51e-013f1c479677','Reuben','Boone','(245) 505-1225','reuben.boone@Nuncquis.co.uk','P.O. Box 987, 3876 Lectus Rd.','Turpis','OR','74060');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('b6860179-461c-469e-a1d9-013f1c479678','Jackson','Hoover','(268) 718-3205','jackson.hoover@egestasurnajusto.org','8615 Nunc Street','Donec','UT','35176');
INSERT INTO [dbo].[Bowler]([Id],[FirstName],[LastName],[Phone],[Email],[Street],[City],[State],[Zip])VALUES('11cd5476-e492-4b6a-aa31-013f1c479679','Rigel','Small','(220) 807-9902','rigel.small@et.org','P.O. Box 838, 619 Molestie St.','Vestibulum','VT','44952');



select * from Bowler;

PRINT '
*** Bowler Score Table Generation ***'

/*** Table [dbo].[Score] ***/
IF OBJECT_ID('[dbo].[Score]', 'U') IS NOT NULL
DROP TABLE [dbo].[Score]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Score](
[Id] [uniqueidentifier] NOT NULL,
[BowlDate] [date] NOT NULL,
[GameNumber] [smallint] NOT NULL,
[Frame01Ball01] [smallint] NUll,
[Frame01Ball02] [smallint] NUll,
[Frame02Ball01] [smallint] NUll,
[Frame02Ball02] [smallint] NUll,
[Frame03Ball01] [smallint] NUll,
[Frame03Ball02] [smallint] NUll,
[Frame04Ball01] [smallint] NUll,
[Frame04Ball02] [smallint] NUll,
[Frame05Ball01] [smallint] NUll,
[Frame05Ball02] [smallint] NUll,
[Frame06Ball01] [smallint] NUll,
[Frame06Ball02] [smallint] NUll,
[Frame07Ball01] [smallint] NUll,
[Frame07Ball02] [smallint] NUll,
[Frame08Ball01] [smallint] NUll,
[Frame08Ball02] [smallint] NUll,
[Frame09Ball01] [smallint] NUll,
[Frame09Ball02] [smallint] NUll,
[Frame10Ball01] [smallint] NUll,
[Frame10Ball02] [smallint] NUll,
[Frame10Ball03] [smallint] NUll,
CONSTRAINT [PK_Score] PRIMARY KEY CLUSTERED
([Id], [BowlDate], [GameNumber]  ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,    IGNORE_DUP_KEY = OFF,
ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


alter table Score
  add constraint fk_ID foreign key (Id)
		references Bowler (Id)
;
GO

INSERT INTO [dbo].[Score]([Id],[BowlDate],[GameNumber],[Frame01Ball01],[Frame01Ball02],[Frame02Ball01],[Frame02Ball02],[Frame03Ball01],[Frame03Ball02],
[Frame04Ball01],[Frame04Ball02],[Frame05Ball01],[Frame05Ball02],[Frame06Ball01],[Frame06Ball02],
[Frame07Ball01],[Frame07Ball02],[Frame08Ball01],[Frame08Ball02],[Frame09Ball01],[Frame09Ball02],[Frame10Ball01],[Frame10Ball02],[Frame10Ball03])
VALUES('11cd5476-e492-4b6a-aa31-013f1c479679','2020-03-16',1,1,1,2,2,3,3,4,4,5,5,4,6,3,7,2,8,1,9,8,2,3);

INSERT INTO [dbo].[Score]([Id],[BowlDate],[GameNumber],[Frame01Ball01],[Frame01Ball02],[Frame02Ball01],[Frame02Ball02],[Frame03Ball01],[Frame03Ball02],
[Frame04Ball01],[Frame04Ball02],[Frame05Ball01],[Frame05Ball02],[Frame06Ball01],[Frame06Ball02],
[Frame07Ball01],[Frame07Ball02],[Frame08Ball01],[Frame08Ball02],[Frame09Ball01],[Frame09Ball02],[Frame10Ball01],[Frame10Ball02],[Frame10Ball03])
VALUES('8f64b183-a881-42f5-9c1d-013f1c479616','2020-03-12',1,1,1,2,2,3,3,4,4,5,5,4,6,3,7,2,8,1,9,1,2,0);

INSERT INTO [dbo].[Score]([Id],[BowlDate],[GameNumber],[Frame01Ball01],[Frame01Ball02],[Frame02Ball01],[Frame02Ball02],[Frame03Ball01],[Frame03Ball02],
[Frame04Ball01],[Frame04Ball02],[Frame05Ball01],[Frame05Ball02],[Frame06Ball01],[Frame06Ball02],
[Frame07Ball01],[Frame07Ball02],[Frame08Ball01],[Frame08Ball02],[Frame09Ball01],[Frame09Ball02],[Frame10Ball01],[Frame10Ball02],[Frame10Ball03])
VALUES('5a2ff04d-e9ae-4de1-9f4e-013f1c479617','2020-04-12',5,5,10,0,0,10,3,7,4,5,9,1,6,3,7,2,8,1,9,1,2,0);

INSERT INTO [dbo].[Score]([Id],[BowlDate],[GameNumber],[Frame01Ball01],[Frame01Ball02],[Frame02Ball01],[Frame02Ball02],[Frame03Ball01],[Frame03Ball02],
[Frame04Ball01],[Frame04Ball02],[Frame05Ball01],[Frame05Ball02],[Frame06Ball01],[Frame06Ball02],
[Frame07Ball01],[Frame07Ball02],[Frame08Ball01],[Frame08Ball02],[Frame09Ball01],[Frame09Ball02],[Frame10Ball01],[Frame10Ball02],[Frame10Ball03])
VALUES('11da4696-cea3-4a6d-8e83-013f1c479618','2020-03-12',1,7,1,2,2,3,3,4,3,5,5,4,6,3,7,2,8,1,9,10,9,1);

INSERT INTO [dbo].[Score]([Id],[BowlDate],[GameNumber],[Frame01Ball01],[Frame01Ball02],[Frame02Ball01],[Frame02Ball02],[Frame03Ball01],[Frame03Ball02],
[Frame04Ball01],[Frame04Ball02],[Frame05Ball01],[Frame05Ball02],[Frame06Ball01],[Frame06Ball02],
[Frame07Ball01],[Frame07Ball02],[Frame08Ball01],[Frame08Ball02],[Frame09Ball01],[Frame09Ball02],[Frame10Ball01],[Frame10Ball02],[Frame10Ball03])
VALUES('11da4696-cea3-4a6d-8e83-013f1c479618','2020-03-12',2,7,1,2,2,3,3,4,3,5,5,4,6,3,7,2,8,1,9,10,9,1);


select * from score as s join Bowler as b
 on s.Id = b.Id;

 
go

--DROP TABLE Score;
;
go