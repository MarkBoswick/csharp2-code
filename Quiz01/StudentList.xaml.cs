﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz01
{
    /// <summary>
    /// Interaction logic for StudentList.xaml
    /// </summary>
    public partial class StudentList : Window
    {
        List<Student> listStudents = new List<Student>();
        public StudentList()
        {
            InitializeComponent();

            listStudents.Add(new Student(1, "Sam", "AAAAAAA"));
            listStudents.Add(new Student(2, "Joe", "BBBBBBBBB"));
            listStudents.Add(new Student(3, "Mary", "CCCCCCCCCCCC"));
            listStudents.Add(new Student(4, "Freddy", "Ddddddd"));
            listStudents.Add(new Student(5, "Beth", "EEEEEEEE"));
            lstBox1.Items.Add(listStudents[0].StudentId + " " + listStudents[0].FirstName + " " + listStudents[0].LastName);
            lstBox1.Items.Add(listStudents[1].StudentId + " " + listStudents[1].FirstName + " " + listStudents[0].LastName);
            lstBox1.Items.Add(listStudents[2].StudentId + " " + listStudents[2].FirstName + " " + listStudents[0].LastName);
            lstBox1.Items.Add(listStudents[3].StudentId + " " + listStudents[3].FirstName + " " + listStudents[0].LastName);
            lstBox1.Items.Add(listStudents[4].StudentId + " " + listStudents[4].FirstName + " " + listStudents[0].LastName);
        }

        //private void btnAdd_Click(object sender, RoutedEventArgs e)
        //{

        //}

        //private void btnSdd_Click(object sender, RoutedEventArgs e)
        //{

        //}

        private void btnStd_Click(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show($"student id = {txtStudent.Text} ");
            //List<Student> listStudents = new List<Student>();

            int i = 0;
            bool j = Int32.TryParse(txtStudent.Text, out i);

            if(j)
            {
             //  lstBox1.Items.Add(listStudents[Int32.Parse(txtStudent.Text)].StudentId + " " + listStudents[Int32.Parse(txtStudent.Text)].FirstName + " " + listStudents[Int32.Parse(txtStudent.Text)].LastName);
                listStudents.Add(new Student( Int32.Parse(txtStudent.Text), "FNamePart01 ", "LNamePart01"));
            } else { MessageBox.Show(" you must enter an integer "); }



            //Student.getStudents();
            //lstBox1.Items.Add(txtStudent.Text);
           // lstBox1.Items.Add(listStudents.ToString());


            

           

            //foreach (var std in listStudents)
            //{
            //    MessageBox.Show(std.FirstName + " " + std.LastName);
            //}


        }
    }
}
