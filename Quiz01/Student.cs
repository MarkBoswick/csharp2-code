﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz01
{
    class Student
    {
        public int StudentId;
        public string FirstName;
        public string LastName;

        public Student(int studentId, string firstName, string lastName)
        {
            this.StudentId = studentId;
            this.FirstName = firstName;
            this.LastName = lastName;
        }
    }
}
