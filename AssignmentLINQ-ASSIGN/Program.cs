﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentLINQ_ASSIGN
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("display the number and frequency of number from giving array.");
            int[] nums = new int[] { 5, 1, 9, 2, 3, 7, 4, 5, 6, 8, 7, 6, 3, 4, 5, 2 };

            //Console.Write("\nLINQ : Display numbers, number*frequency and frequency : ");
            //Console.Write("\n-------------------------------------------------------\n");
            Console.Write("The numbers in the array are : \n");
            Console.Write("  5, 1, 9, 2, 3, 7, 4, 5, 6, 8, 7, 6, 3, 4, 5, 2 \n\n");


            var m = from x in nums
                    group x by x into y
                    select y;
            
            foreach (var arrElement in m)
            {
                Console.WriteLine("Number " + arrElement.Key + " appears " + arrElement.Count() + " times");
            }
            Console.WriteLine();




            Console.WriteLine("------------------------------------------");
            Console.WriteLine("display the top nth records");
            Console.Write("How many records do you want to display ? : ");
            string strTakeInput = Console.ReadLine();
            int intTakeInput;

            bool isParsable = Int32.TryParse(strTakeInput, out intTakeInput);
            if (isParsable)
            { 
                //Console.WriteLine(intTakeInput);

                int[] array = new int[] { 5, 7, 13, 24, 6, 9, 8, 7 };
                var topThree = (from i in array
                            orderby i descending
                            select i).Take(intTakeInput);

                foreach (var x in topThree)
                {
                    Console.Write(x + " ");
                }
                Console.WriteLine("");

            }
            else
            {
                Console.WriteLine("You must enter a valid integer");
            }
                

           
        }
    }
}
